package com.idplus.flyco2tracker

class Constants {

    companion object {
        const val URL_BASE = "https://api.wikimedia.org/"
        const val COMFORT_CATEGORY_NORMAL = "normal"
        const val COMFORT_CATEGORY_BUSINESS = "business"
        const val COMFORT_CATEGORY_EXTRA = "comfort"
    }
}